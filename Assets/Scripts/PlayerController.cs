﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private GameObject playerPrefab;
    private Dictionary<string, Player> idPlayers = new Dictionary<string, Player>(); 

    public void AppearPlayer()
    {
        Player player = ComponentUtil.InstantiateTo<Player>(this.gameObject, playerPrefab);
        player.Init();
        idPlayers.Add(Guid.NewGuid().ToString(), player);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
