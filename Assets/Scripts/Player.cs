﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class Player : MonoBehaviour
{
    [SerializeField] private RuntimeAnimatorController characterAnimationController;
    // TODO あとでGameControllerより取得する
    [SerializeField] private GameObject characterObject;
    [SerializeField] private Bounds characterColliderBound;
    private Animator characterAnimator;

    public void Init()
    {
        AnimatorOverrideController overrideAnimetorController = new AnimatorOverrideController();
        overrideAnimetorController.runtimeAnimatorController = characterAnimationController;
        characterAnimator = characterObject.GetComponent<Animator>();
        characterAnimator.runtimeAnimatorController = overrideAnimetorController;

        //        BoxCollider collider = characterObject.AddComponent<BoxCollider>();
        //        collider.center = characterColliderBound.center;
        //        collider.size = characterColliderBound.extents * 2;

        //Rigidbody rigidBody = characterObject.AddComponent<Rigidbody>();
        ThirdPersonCharacter thirdPersonCharacter = characterObject.AddComponent<ThirdPersonCharacter>();
        CapsuleCollider capsuleCollider = thirdPersonCharacter.GetComponent<CapsuleCollider>();
        capsuleCollider.center = characterColliderBound.center;
        capsuleCollider.height = Mathf.Abs(characterColliderBound.extents.y);
        capsuleCollider.radius = Mathf.Abs(characterColliderBound.extents.x);

        characterObject.AddComponent<ThirdPersonUserControl>();
    }


    public void Shoot()
    {

    }

    public void DropOut()
    {

    }

    public void Hit()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
