﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalcColliderUtil : MonoBehaviour
{
    public static BoxCollider AttachMaxVolumnBoxCollider(GameObject go)
    {
        Dictionary<GameObject, Renderer> objectRenderers = new Dictionary<GameObject, Renderer>();
        List<SkinnedMeshRenderer> renderers = FindAllCompomentInChildren<SkinnedMeshRenderer>(go.transform);
        if (renderers == null || renderers.Count <= 0) return null;
        SkinnedMeshRenderer skinnedMeshRenderer = null;
        float maxVolumn = float.MinValue;
        for (int j = 0; j < renderers.Count; ++j)
        {
            Vector3 cubeSize = renderers[j].bounds.extents * 2;
            float volumn = cubeSize.x * cubeSize.y * cubeSize.z;
            if (maxVolumn < volumn)
            {
                maxVolumn = volumn;
                skinnedMeshRenderer = renderers[j];
            }
        }
        Bounds bounds = skinnedMeshRenderer.bounds;
        BoxCollider boxCollider = go.GetComponent<BoxCollider>();
        if (boxCollider.Equals(null))
        {
            boxCollider = go.AddComponent<BoxCollider>();
        }
        boxCollider.center = bounds.center;
        boxCollider.size = bounds.extents * 2;
        return boxCollider;
    }

    private static List<T> FindAllCompomentInChildren<T>(Transform root) where T : class
    {
        List<T> compoments = new List<T>();
        for (int i = 0; i < root.childCount; ++i)
        {
            Transform t = root.GetChild(i);
            T compoment = t.GetComponent<T>();
            if (!compoment.Equals(null))
            {
                compoments.Add(compoment);
            }

            // It seems that null of GetCompoment differs from return null ...
            List<T> childCompoments = FindAllCompomentInChildren<T>(t);
            compoments.AddRange(childCompoments);
        }

        return compoments;
    }
}
