﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageController : MonoBehaviour
{
    [SerializeField] private GameObject stagePrefab;
    [SerializeField] private PlayerController playerController;

    // Start is called before the first frame update
    void Start()
    {
        GenerateStage();
    }

    public void GenerateStage()
    {
        ComponentUtil.InstantiateTo(this.gameObject, stagePrefab);
        playerController.AppearPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DropPlayer()
    {

    }
}
